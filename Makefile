# dumbwm - dynamic window manager
#   (C)opyright MMVI Anselm R. Garbe

include config.mk

SRC =  client.c event.c main.c util.c
OBJ = ${SRC:.c=.o}
MAN1 = dumbwm.1 
BIN = dumbwm

all: config dumbwm
	@echo finished

config:
	@echo dumbwm build options:
	@echo "LIBS     = ${LIBS}"
	@echo "CFLAGS   = ${CFLAGS}"
	@echo "LDFLAGS  = ${LDFLAGS}"
	@echo "CC       = ${CC}"

.c.o:
	@echo CC $<
	@${CC} -c ${CFLAGS} $<

${OBJ}: dumbwm.h

dumbwm: ${OBJ}
	@echo LD $@
	@${CC} -o $@ ${OBJ} ${LDFLAGS}

clean:
	rm -f dumbwm *.o core

dist: clean
	mkdir -p dumbwm-${VERSION}
	cp -R Makefile README LICENSE config.mk *.h *.c ${MAN1} dumbwm-${VERSION}
	tar -cf dumbwm-${VERSION}.tar dumbwm-${VERSION}
	gzip dumbwm-${VERSION}.tar
	rm -rf dumbwm-${VERSION}

install: all
	@mkdir -p ${DESTDIR}${PREFIX}/bin
	@cp -f ${BIN} ${DESTDIR}${PREFIX}/bin
	@echo installed executable files to ${DESTDIR}${PREFIX}/bin

uninstall:
	for i in ${BIN}; do \
		rm -f ${DESTDIR}${PREFIX}/bin/`basename $$i`; \
	done
	for i in ${MAN1}; do \
		rm -f ${DESTDIR}${MANPREFIX}/man1/`basename $$i`; \
	done
