# Customize to fit your system

# paths
PREFIX = /usr
CONFPREFIX = ${PREFIX}/etc

X11INC = /usr/X11R6/include
X11LIB = /usr/X11R6/lib


# includes and libs
LIBS = -L${PREFIX}/lib -L/usr/lib -lc -L${X11LIB} -lX11

# Linux/BSD
CFLAGS = -Os -I.  -pedantic -Wall  -Wno-deprecated-declarations -I${PREFIX}/include -I/usr/include -I${X11INC} 
LDFLAGS = ${LIBS}
#CFLAGS = -g -Wall -O2 -I. -I${PREFIX}/include -I/usr/include -I${X11INC} \
#	-DVERSION=\"${VERSION}\"
#LDFLAGS = -g ${LIBS}


# Solaris
#CFLAGS = -fast -xtarget=ultra ${INCLUDES} -DVERSION=\"${VERSION}\"
#LIBS += -lnsl -lsocket

AR = ar cr
CC = cc
RANLIB = ranlib
