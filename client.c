/*
 * (C)opyright MMVI Anselm R. Garbe <garbeam at gmail dot com>
 * See LICENSE file for license details.
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <X11/Xutil.h>

#include "dumbwm.h"

void (*arrange)(Arg *) = tiling;


static Client *
next(Client *c)
{
	for(; c ; c = c->next);
	return c;
}

void
max(Arg *arg)
{
	if(!sel)
		return;
	sel->x = sx;
	sel->y = sy ;
	sel->w = sw - 2 * sel->border;
	sel->h = sh - 2 * sel->border;
	craise(sel);
	resize(sel, False);
}

void
view(Arg *arg)
{
	Client *c;

	tsel = arg->i;
	arrange(NULL);

}
void
tiling(Arg *arg)
{
	Client *c;
	int n, i, w, h;

	w = sw - mw;
	arrange = tiling;
	for(n = 0, c = clients; c; c = c->next)
			n++;

	if(n > 1)
		h = (sh ) / (n - 1);
	else
		h = sh;

	for(i = 0, c = clients; c; c = c->next) {
			if(c->floating) {
				craise(c);
				resize(c, True);
				continue;
			}
			if(n == 1) {
				c->x = sx;
				c->y = sy ;
				c->w = sw - 2 * c->border;
				c->h = sh - 2 * c->border ;
			}
			else if(i == 0) {
				c->x = sx;
				c->y = sy ;
				c->w = mw - 2 * c->border;
				c->h = sh - 2 * c->border ;
			}
			else {
				c->x = sx + mw;
				c->y = sy + (i - 1) * h ;
				c->w = w - 2 * c->border;
				c->h = h - 2 * c->border;
			}
			resize(c, False);
			i++;
		}
	if(!sel ) {
		if((sel = next(clients))) {
			craise(sel);
			focus(sel);
		}
	}
}

void
prevc(Arg *arg)
{
	Client *c;

	if(!sel)
		return;

	if((c = sel->revert ? sel->revert : NULL)) {
		craise(c);
		focus(c);
	}
}

void
nextc(Arg *arg)
{
	Client *c;
   
	if(!sel)
		return;

	if(!(c = next(sel->next)))
		c = next(clients);
	if(c) {
		craise(c);
		c->revert = sel;
		focus(c);
	}
}

void
ckill(Arg *arg)
{
	if(!sel)
		return;

	else
		XKillClient(dpy, sel->win);
}

void
update_size(Client *c)
{
	XSizeHints size;
	long msize;
	if(!XGetWMNormalHints(dpy, c->win, &size, &msize) || !size.flags)
		size.flags = PSize;
	c->flags = size.flags;
	if(c->flags & PBaseSize) {
		c->basew = size.base_width;
		c->baseh = size.base_height;
	}
	else
		c->basew = c->baseh = 0;
	if(c->flags & PResizeInc) {
		c->incw = size.width_inc;
		c->inch = size.height_inc;
	}
	else
		c->incw = c->inch = 0;
	if(c->flags & PMaxSize) {
		c->maxw = size.max_width;
		c->maxh = size.max_height;
	}
	else
		c->maxw = c->maxh = 0;
	if(c->flags & PMinSize) {
		c->minw = size.min_width;
		c->minh = size.min_height;
	}
	else
		c->minw = c->minh = 0;
	if(c->flags & PWinGravity)
		c->grav = size.win_gravity;
	else
		c->grav = NorthWestGravity;
}

void
craise(Client *c)
{
	XRaiseWindow(dpy, c->win);
}

void
lower(Client *c)
{
	XLowerWindow(dpy, c->win);
}

void
focus(Client *c)
{
	Client *old = sel;
	XEvent ev;

	XFlush(dpy);
	sel = c;
	XSetInputFocus(dpy, c->win, RevertToPointerRoot, CurrentTime);
	XFlush(dpy);
	while(XCheckMaskEvent(dpy, EnterWindowMask, &ev));
}

void
manage(Window w, XWindowAttributes *wa)
{
	Client *c, **l;
	XSetWindowAttributes twa;
	Window trans;

	c = emallocz(sizeof(Client));
	c->win = w;
	c->tx = c->x = wa->x;
	c->ty = c->y = wa->y;
	c->tw = c->w = wa->width;
	c->h = wa->height;
	c->border = 0;
	update_size(c);
	XSelectInput(dpy, c->win,
			StructureNotifyMask | PropertyChangeMask | EnterWindowMask);
	XGetTransientForHint(dpy, c->win, &trans);
	twa.override_redirect = 1;
	twa.background_pixmap = ParentRelative;
	twa.event_mask = ExposureMask;


	for(l = &clients; *l; l = &(*l)->next);
	c->next = *l; /* *l == nil */
	*l = c;

	XGrabButton(dpy, Button1, Mod1Mask, c->win, False, ButtonPressMask,
			GrabModeAsync, GrabModeSync, None, None);
	XGrabButton(dpy, Button2, Mod1Mask, c->win, False, ButtonPressMask,
			GrabModeAsync, GrabModeSync, None, None);
	XGrabButton(dpy, Button3, Mod1Mask, c->win, False, ButtonPressMask,
			GrabModeAsync, GrabModeSync, None, None);

	if(!c->floating)
		c->floating = trans
			|| ((c->maxw == c->minw) && (c->maxh == c->minh));

	arrange(NULL);
	/* mapping the window now prevents flicker */
		XMapRaised(dpy, c->win);
		focus(c);
}

void
gravitate(Client *c, Bool invert)
{
	int dx = 0, dy = 0;

	switch(c->grav) {
	case StaticGravity:
	case NorthWestGravity:
	case NorthGravity:
	case NorthEastGravity:
		dy = c->border;
		break;
	case EastGravity:
	case CenterGravity:
	case WestGravity:
		dy = -(c->h / 2) + c->border;
		break;
	case SouthEastGravity:
	case SouthGravity:
	case SouthWestGravity:
		dy = -c->h;
		break;
	default:
		break;
	}

	switch (c->grav) {
	case StaticGravity:
	case NorthWestGravity:
	case WestGravity:
	case SouthWestGravity:
		dx = c->border;
		break;
	case NorthGravity:
	case CenterGravity:
	case SouthGravity:
		dx = -(c->w / 2) + c->border;
		break;
	case NorthEastGravity:
	case EastGravity:
	case SouthEastGravity:
		dx = -(c->w + c->border);
		break;
	default:
		break;
	}

	if(invert) {
		dx = -dx;
		dy = -dy;
	}
	c->x += dx;
	c->y += dy;
}


void
resize(Client *c, Bool inc)
{
	XConfigureEvent e;

	if(inc) {
		if(c->incw)
			c->w -= (c->w - c->basew) % c->incw;
		if(c->inch)
			c->h -= (c->h - c->baseh) % c->inch;
	}
	if(c->x > sw) /* might happen on restart */
		c->x = sw - c->w;
	if(c->y > sh)
		c->ty = c->y = sh - c->h;
	if(c->minw && c->w < c->minw)
		c->w = c->minw;
	if(c->minh && c->h < c->minh)
		c->h = c->minh;
	if(c->maxw && c->w > c->maxw)
		c->w = c->maxw;
	if(c->maxh && c->h > c->maxh)
		c->h = c->maxh;
	XSetWindowBorderWidth(dpy, c->win, 1);
	XMoveResizeWindow(dpy, c->win, c->x, c->y, c->w, c->h);
	e.type = ConfigureNotify;
	e.event = c->win;
	e.window = c->win;
	e.x = c->x;
	e.y = c->y;
	e.width = c->w;
	e.height = c->h;
	e.border_width = c->border;
	e.above = None;
	e.override_redirect = False;
	XSendEvent(dpy, c->win, False, StructureNotifyMask, (XEvent *)&e);
	XFlush(dpy);
}

static int
dummy_error_handler(Display *dsply, XErrorEvent *err)
{
	return 0;
}

void
unmanage(Client *c)
{
	Client **l;

	XGrabServer(dpy);
	XSetErrorHandler(dummy_error_handler);

	XUngrabButton(dpy, AnyButton, AnyModifier, c->win);

	for(l = &clients; *l && *l != c; l = &(*l)->next);
	*l = c->next;
	for(l = &clients; *l; l = &(*l)->next)
		if((*l)->revert == c)
			(*l)->revert = NULL;
	if(sel == c)
		sel = sel->revert ? sel->revert : clients;

	free(c);

	XFlush(dpy);
	XSetErrorHandler(error_handler);
	XUngrabServer(dpy);
	arrange(NULL);
	if(sel)
		focus(sel);
}



Client *
getclient(Window w)
{
	Client *c;
	for(c = clients; c; c = c->next)
		if(c->win == w)
			return c;
	return NULL;
}

