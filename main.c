/*
 * (C)opyright MMVI Anselm R. Garbe <garbeam at gmail dot com>
 * See LICENSE file for license details.
 */

#include <errno.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <X11/cursorfont.h>
#include <X11/Xproto.h>

#include "dumbwm.h"

/********** CUSTOMIZE **********/


/********** CUSTOMIZE **********/

/* X structs */
Display *dpy;
Window root;
Cursor cursor[CurLast];
Bool running = True;
Bool issel;

int tsel = Tdev; /* default tag */
int screen, sx, sy, sw, sh, mw;
char stext[1024];

Client *clients = NULL;
Client *sel = NULL;

static Bool other_wm_running;
static const char version[] =
	"dumbwm-"  ", (C)opyright MMVI Anselm R. Garbe\n";
static int (*x_error_handler) (Display *, XErrorEvent *);

static void
usage() {	error("usage: dumbwm [-v]\n"); }

static void
scan_wins()
{
	unsigned int i, num;
	Window *wins;
	XWindowAttributes wa;
	Window d1, d2;

	if(XQueryTree(dpy, root, &d1, &d2, &wins, &num)) {
		for(i = 0; i < num; i++) {
			if(!XGetWindowAttributes(dpy, wins[i], &wa))
				continue;
			if(wa.override_redirect || XGetTransientForHint(dpy, wins[i], &d1))
				continue;
			if(wa.map_state == IsViewable)
				manage(wins[i], &wa);
		}
	}
	if(wins)
		XFree(wins);
}


/*
 * There's no way to check accesses to destroyed windows, thus
 * those cases are ignored (especially on UnmapNotify's).
 * Other types of errors call Xlib's default error handler, which
 * calls exit().
 */
int
error_handler(Display *dpy, XErrorEvent *error)
{
	if(error->error_code == BadWindow
			|| (error->request_code == X_SetInputFocus
				&& error->error_code == BadMatch)
			|| (error->request_code == X_PolyText8
				&& error->error_code == BadDrawable)
			|| (error->request_code == X_PolyFillRectangle
				&& error->error_code == BadDrawable)
			|| (error->request_code == X_PolySegment
				&& error->error_code == BadDrawable)
			|| (error->request_code == X_ConfigureWindow
				&& error->error_code == BadMatch)
			|| (error->request_code == X_GrabKey
				&& error->error_code == BadAccess))
		return 0;
	fprintf(stderr, "dumbwm: fatal error: request code=%d, error code=%d\n",
			error->request_code, error->error_code);
	return x_error_handler(dpy, error); /* may call exit() */
}

/*
 * Startup Error handler to check if another window manager
 * is already running.
 */
static int
startup_error_handler(Display *dpy, XErrorEvent *error)
{
	other_wm_running = True;
	return -1;
}

static void
cleanup()
{
	while(sel) {
		resize(sel, True);
		unmanage(sel);
	}
	XSetInputFocus(dpy, PointerRoot, RevertToPointerRoot, CurrentTime);
}

void
quit(Arg *arg)
{
	running = False;
}

int
main(int argc, char *argv[])
{
	int i, n;
	fd_set rd;
	XSetWindowAttributes wa;
	unsigned int mask;
	Bool readstdin = True;
	Window w;
	XEvent ev;

	for(i = 1; (i < argc) && (argv[i][0] == '-'); i++) {
		switch (argv[i][1]) {
		case 'v':
			fprintf(stdout, "%s", version);
			exit(0);
			break;
		default:
			usage();
			break;
		}
	}

	dpy = XOpenDisplay(0);
	if(!dpy)
		error("dumbwm: cannot connect X server\n");

	screen = DefaultScreen(dpy);
	root = RootWindow(dpy, screen);

	/* check if another WM is already running */
	other_wm_running = False;
	XSetErrorHandler(startup_error_handler);
	/* this causes an error if some other WM is running */
	XSelectInput(dpy, root, SubstructureRedirectMask);
	XFlush(dpy);

	if(other_wm_running)
		error("dumbwm: another window manager is already running\n");

	XSetErrorHandler(0);
	x_error_handler = XSetErrorHandler(error_handler);


	/* init cursors */
	cursor[CurNormal] = XCreateFontCursor(dpy, XC_left_ptr);
	cursor[CurResize] = XCreateFontCursor(dpy, XC_sizing);
	cursor[CurMove] = XCreateFontCursor(dpy, XC_fleur);


	/* style */

	sx = sy = 0;
	sw = DisplayWidth(dpy, screen);
	sh = DisplayHeight(dpy, screen);
	mw = (sw * MASTERW) / 100;

	wa.override_redirect = 1;
	wa.background_pixmap = ParentRelative;
	wa.event_mask = ButtonPressMask | ExposureMask;


	issel = XQueryPointer(dpy, root, &w, &w, &i, &i, &i, &i, &mask);

	wa.event_mask = SubstructureRedirectMask | EnterWindowMask \
					| LeaveWindowMask;
	wa.cursor = cursor[CurNormal];

	XChangeWindowAttributes(dpy, root, CWEventMask | CWCursor, &wa);

	strcpy(stext, "dumbwm-");
	scan_wins();

	/* main event loop, reads status text from stdin as well */
Mainloop:
	while(running) {
		FD_ZERO(&rd);
		if(readstdin)
			FD_SET(STDIN_FILENO, &rd);
		FD_SET(ConnectionNumber(dpy), &rd);

		i = select(ConnectionNumber(dpy) + 1, &rd, 0, 0, 0);
		if(i == -1 && errno == EINTR)
			continue;
		if(i < 0)
			error("select failed\n");
		else if(i > 0) {
			if(FD_ISSET(ConnectionNumber(dpy), &rd)) {
				while(XPending(dpy)) {
					XNextEvent(dpy, &ev);
					if(handler[ev.type])
						(handler[ev.type])(&ev); /* call handler */
				}
			}
			if(readstdin && FD_ISSET(STDIN_FILENO, &rd)) {
				i = n = 0;
				for(;;) {
					if((i = getchar()) == EOF) {
						/* broken pipe/end of producer */
						readstdin = False;
						strcpy(stext, "broken pipe");
						goto Mainloop;
					}
					if(i == '\n' || n >= sizeof(stext) - 1)
						break;
					stext[n++] = i;
				}
				stext[n] = 0;
			}
		}
	}

	cleanup();
	XCloseDisplay(dpy);

	return 0;
}
